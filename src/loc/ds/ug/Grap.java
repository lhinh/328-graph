package loc.ds.ug;

import java.util.Set;
import java.util.Stack;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Iterator;

public class Grap<T> {
	private Set<T> vertices;
	private Set<Pair<T,T>> edges;
	private Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> connected;
	
	public Grap() {
		this.vertices = new HashSet<T>();
		this.edges = new HashSet<Pair<T,T>>();
		this.connected = new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
	}
	
	public Grap(Set<T> vertices, Set<Pair<T,T>> edges) {
		this.vertices = new HashSet<T>(vertices);
		this.edges = new HashSet<Pair<T,T>>(edges);
		this.connected = new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
		// Include a function to find components
	}
	
	public void addNode(T node) {this.vertices.add(node);}
	
	public void addEdge(Pair<T,T> edge) throws Exception {
		boolean isVertex0InGraph = this.vertices.contains(edge.getElement0());
		boolean isVertex1InGraph = this.vertices.contains(edge.getElement1());
		if (isVertex0InGraph && isVertex1InGraph) {
			this.edges.add(edge);
		} else {
			String notInGraph = "";
			if (!isVertex0InGraph)
				notInGraph += edge.getElement0().toString() + " ";
			if (!isVertex1InGraph)
				notInGraph += edge.getElement1().toString();
			throw new Exception(String.format("The following nodes are not in the graph: %s", notInGraph));
		}
	}
	
	public boolean isConnected() {
		this.connected.putAll(breadthFirstSearch(this));
		return this.connected.size() == 1;
	}

	public int numConnComponents() {
		return this.connected.size();
	}
	
	public Grap<T>[] connectedComponents() {
		if (!this.isConnected()) {
			Grap<T>[] grappies = new Grap[numConnComponents()];
			for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : this.connected.entrySet()) {
				for (Map.Entry<Set<T>, Set<Pair<T,T>>> g : component.getValue().entrySet()) {
					grappies[component.getKey()-1] = new Grap(g.getKey(), g.getValue());
				}
			}
			return grappies;
		} else
			return null;
	}
	
	public void printBFS() {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components = breadthFirstSearch(this);
		for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : components.entrySet()) 
			for (Set<T> nodes : component.getValue().keySet())
				System.out.printf("Tree: %s\n", nodes.toString());
	}
	public void printDFS() {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components = depthFirstSearch(this);
		for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : components.entrySet()) 
			for (Set<T> nodes : component.getValue().keySet())
				System.out.printf("Tree: %s\n", nodes.toString());
	}
	protected Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> breadthFirstSearch(Grap<T> g) {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components =
				new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();

		Queue<T> queue = new LinkedList<T>();
		Map<T, Boolean> visited = new HashMap<T, Boolean>();
		
		g.getVertices().forEach((T) -> {
			visited.put(T, false);
		});
		
		Iterator<Entry<T, Boolean>> vItr = visited.entrySet().iterator();
		while (vItr.hasNext()) {
			Entry<T, Boolean> u = vItr.next();
			T node = u.getKey();
			Boolean isVisited = u.getValue();
			Set<T> newVertices = new HashSet<T>();
			Set<Pair<T,T>> newEdges = new HashSet<Pair<T,T>>();
			Map<Set<T>, Set<Pair<T,T>>> spanningTree = new HashMap<Set<T>, Set<Pair<T,T>>>();
			if (!isVisited) {
				isVisited = true;
				visited.put(node, isVisited);
				queue.offer(node);
				newVertices.add(node);
				
				while (!queue.isEmpty()) {
					node = queue.poll();
					for (Pair<T,T> p : g.getEdges()) {
						T v0 = p.getElement0();
						T v1 = p.getElement1();
						if (v0 == node) {
							if (!visited.get(v1)) {
								visited.put(v1, true);
								queue.offer(v1);
								newVertices.add(v1);
								newEdges.add(p);
							} else if (!newEdges.contains(p))
								newEdges.add(p);
						}
						if (v1 == node) {
							if (!visited.get(v0)) {
								visited.put(v0, true);
								queue.offer(v0);
								newVertices.add(v0);
								newEdges.add(p);
							} else if (!newEdges.contains(p))
								newEdges.add(p);
						}
					}
				}
				spanningTree.put(newVertices, newEdges);
				if (!components.containsValue(spanningTree)) {
					components.put(components.size()+1, spanningTree);
				}
			}
		}
		return components;
	}
	protected Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> depthFirstSearch(Grap<T> g) {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components =
				new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
		
		Stack<T> stack = new Stack<T>();
		Map<T, Boolean> visited = new HashMap<T, Boolean>();
		
		g.getVertices().forEach((T) -> {
			visited.put(T, false);
		});
		
		Iterator<Entry<T, Boolean>> vItr = visited.entrySet().iterator();
		while (vItr.hasNext()) {
			Entry<T, Boolean> u = vItr.next();
			T node = u.getKey();
			Boolean isVisited = u.getValue();
			Set<T> newVertices = new HashSet<T>();
			Set<Pair<T,T>> newEdges = new HashSet<Pair<T,T>>();
			Map<Set<T>, Set<Pair<T,T>>> spanningTree = new HashMap<Set<T>, Set<Pair<T,T>>>();
			
			if (!isVisited) {
				isVisited = true;
				visited.put(node, isVisited);
				stack.push(node);
				newVertices.add(node);
				
				while (!stack.isEmpty()) {
					node = stack.peek();
					boolean added = false;
					for (Pair<T,T> p : g.getEdges()) {
						T w = p.getElement0();
						T v = p.getElement1();
						if (w == node) {
							if (!visited.get(v)) {
								visited.put(v, true);
								stack.push(v);
								added = true;
								newVertices.add(v);
								newEdges.add(p);
							} else if (!newEdges.contains(p))
								newEdges.add(p);
						} else if (v == node) {
							if (!visited.get(w)) {
								visited.put(w, true);
								stack.push(w);
								added = true;
								newVertices.add(w);
								newEdges.add(p);
							} else if (!newEdges.contains(p))
								newEdges.add(p);
						}
					}
					if (!added)
						stack.pop();
				}
				spanningTree.put(newVertices, newEdges);
				if (!components.containsValue(spanningTree)) {
					components.put(components.size()+1, spanningTree);
				}
			}
		}
		return components;
	}
	public String toString() {
		String result = "";
		String vertices = "Nodes: ";
		String edges = "Edges: ";
		
		for (T v : this.vertices) {
			vertices += String.format("%s, ", v.toString());
		}
		for (Pair<T,T> e : this.edges) {
			edges += String.format("%s, ", e.toString());
		}
		result += vertices.substring(0, vertices.length()-2)
				+ "\n" + edges.substring(0, edges.length()-2);
		return result;
	}
	protected Set<T> getVertices() {return this.vertices;}
	protected Set<Pair<T,T>> getEdges() {return this.edges;}
	protected Map<Integer, Map<Set<T>, Set<Pair<T, T>>>> getConnected() {return this.connected;}
	
}
