package loc.ds.ug;

import java.util.HashSet;
import java.util.Set;

public class main {
	
	public static <T> Set<T> createVertices(T[] d) {
		Set<T> result = new HashSet<T>();
		for (T in : d)
			result.add(in);
		return result;
	}
	
	public static Set<Pair<Character, Character>> partOneTwoEdges() {
		Set<Pair<Character, Character>> result = new HashSet<Pair<Character, Character>>();
		result.add(new Pair<Character, Character>('a','b'));
		result.add(new Pair<Character, Character>('a','c'));
		result.add(new Pair<Character, Character>('b','c'));
		result.add(new Pair<Character, Character>('d','e'));
		result.add(new Pair<Character, Character>('f','g'));
		result.add(new Pair<Character, Character>('g','h'));
		return result;
	}
	public static Set<Pair<Character, Character>> partThreeGEdges() {
		Set<Pair<Character, Character>> result = new HashSet<Pair<Character, Character>>();
		result.add(new Pair<Character, Character>('a','b'));
		result.add(new Pair<Character, Character>('b','c'));
		result.add(new Pair<Character, Character>('b','d'));
		result.add(new Pair<Character, Character>('c','d'));
		result.add(new Pair<Character, Character>('d','e'));
		result.add(new Pair<Character, Character>('e','a'));
		return result;
	}
	public static Set<Pair<Character, Character>> partThreeHEdges() {
		Set<Pair<Character, Character>> result = new HashSet<Pair<Character, Character>>();
		result.add(new Pair<Character, Character>('b','a'));
		result.add(new Pair<Character, Character>('b','c'));
		result.add(new Pair<Character, Character>('c','d'));
		result.add(new Pair<Character, Character>('d','b'));
		result.add(new Pair<Character, Character>('d','e'));
		result.add(new Pair<Character, Character>('e','a'));
		return result;
	}
	
	public static <T> void testOneAll(Set<T> vertices, Set<Pair<T,T>> edges) throws Exception {
		Grap<T> g = new Grap<T>();
		vertices.forEach((T) -> {g.addNode(T);});
		edges.forEach((T) -> {
			try {
				g.addEdge(T);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println(g.toString());
		g.printDFS();
		System.out.printf("Is connected? %s\n", g.isConnected());
		System.out.printf("Number of connected components: %s\n", g.numConnComponents());
	}
	
	public static <T> void testTwoAll(Set<T> vertices, Set<Pair<T,T>> edges) throws Exception {
		Grap<T> g = new Grap<T>(vertices, edges);
		System.out.println(g.toString());
		g.printDFS();
		System.out.printf("Is connected? %s\n", g.isConnected());
		System.out.printf("Number of connected components: %s\n", g.numConnComponents());
		Grap<T>[] subG = g.connectedComponents();
		for (int i = 0; i < subG.length; i++) {
			System.out.printf("Connected Component #%s:\n", i+1);
			System.out.println(subG[i].toString());
		}
	}
	
	public static void testThreeAll() throws Exception {
		Character[] data = {'a','b','c','d','e'};
		Set<Character> vertices = createVertices(data);
		Set<Pair<Character, Character>> gEdges = partThreeGEdges();
		Set<Pair<Character, Character>> hEdges = partThreeHEdges();
		DirectedGrap<Character> dG = new DirectedGrap<Character>(vertices, gEdges);
		DirectedGrap<Character> hG = new DirectedGrap<Character>(vertices, hEdges);
		System.out.printf("Graph G: %s\n", dG.isConnected());
		System.out.printf("Graph H:%s\n", hG.isConnected());
		System.out.printf("Graph G: %s\n", dG.isStronglyConnected());
		System.out.printf("Graph H:%s\n", hG.isStronglyConnected());
		dG.printBFS();
		hG.printBFS();
		DirectedGrap<Character>[] subHG = hG.connectedComponents();
		for (int i = 0; i < subHG.length; i++) {
			System.out.printf("Strongly Connected Component #%s:\n", i+1);
			System.out.println(subHG[i].toString());
		}
	}


	public static void main(String[] args) {
		Character[] data = {'a','b','c','d','e','f','g','h'};
		Set<Character> vertices = createVertices(data);
		Set<Pair<Character, Character>> edges = partOneTwoEdges();
		try {
			testOneAll(vertices, edges);
			System.out.println();
			testTwoAll(vertices, edges);
			System.out.println();
			testThreeAll();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

}
