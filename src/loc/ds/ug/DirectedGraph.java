package loc.ds.ug;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class DirectedGraph<T> extends Graph<T> {
	//BEGIN Constructors
	public DirectedGraph() {
		super();
	}
	
	public DirectedGraph(Set<T> vertices, Set<Pair<T,T>> edges) {
		super(vertices, edges);
	}
	//END Constructors
	
	//BEGIN Getters
	public boolean isConnected() {
		return (isStronglyConnected()) ? true : false;
	}
	
	public boolean isStronglyConnected() {
		methodOne(super.getVertices());
//		methodTwo();
		return super.numConnComponents() == 1;
	}
	
	public int numConnComponents() {
		methodOne(super.getVertices());
//		methodTwo();
		return super.numConnComponents();
	}
	
	@SuppressWarnings("unchecked")
	public DirectedGraph<T>[] connectedComponents() {
		if (isStronglyConnected()) {
			int size = numConnComponents();
			DirectedGraph<T>[] grap = new DirectedGraph[size];
			Graph<T>[] g = super.connectedComponents();
			for (int i = 0; i < size; i++) {
				Set<T> vertices = g[i].getVertices();
				Set<Pair<T,T>> edges = g[i].getEdges();
				grap[i] = new DirectedGraph<T>(vertices, edges);
			}
			return grap;
		}
		return null;
	}
	//END Getters
	
	//BEGIN HELPER FUNCTIONS
	private HashMap<Integer, HashMap<T, Boolean>> methodOne(Set<T> vertices) {
		int size = vertices.size();
		HashMap<Integer, HashMap<T, Boolean>> spanningForest = new HashMap<Integer, HashMap<T, Boolean>>();
		HashMap<T, Boolean> spanningTree = new HashMap<T, Boolean>();
		for (int i = 0; i < size; i++) {
			Set<T> rotateVertices = new HashSet<T>();
			Iterator<T> itr = vertices.iterator();
			T head = null;
			if (itr.hasNext()) {
				head = itr.next();
				vertices.remove(head);
				rotateVertices.addAll(vertices);
				rotateVertices.add(head);
				spanningTree.putAll(super.depthFirstSearch(rotateVertices));
				super.addConnected(spanningTree);
				spanningForest.put(spanningForest.size()+1, spanningTree);
			}
		}
		return spanningForest;
	}
	private Set<Pair<T,T>> reverseEdges(Set<Pair<T,T>> edges) {
		Set<Pair<T,T>> reversed = new HashSet<Pair<T,T>>();
		
		for (Pair<T,T> p : edges) {
			T element0 = p.getElement1();
			T element1 = p.getElement0();
			Pair<T,T> edge = new Pair<T,T>(element0, element1);
			reversed.add(edge);
		}
		return reversed;
	}
	private boolean methodTwo() {
		super.depthFirstSearch(super.getVertices());
		Set<Pair<T,T>> reversedEdges = reverseEdges(super.getEdges());
		DirectedGraph hparG = new DirectedGraph(super.getVertices(), reversedEdges);
		hparG.depthFirstSearch(getVertices());
		return super.numConnComponents() == hparG.numConnComponents();
	}
	//END HELPER FUNCTIONS
}
