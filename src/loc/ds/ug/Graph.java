package loc.ds.ug;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Graph<T> {
	private Set<T> vertices;
	private Set<Pair<T, T>> edges;
	private Map<Integer, HashMap<T, Boolean>> connected;

	//BEGIN Constructors
	public Graph() {
		this.vertices = new HashSet<T>();
		this.edges = new HashSet<Pair<T, T>>();
		this.connected = new HashMap<Integer, HashMap<T, Boolean>>();
	}

	public Graph(Set<T> V, Set<Pair<T, T>> E) {
		this.vertices = new HashSet<T>(V);
		this.edges = new HashSet<Pair<T, T>>(E);
		this.connected = new HashMap<Integer, HashMap<T, Boolean>>();
	}
	//END Constructors

	//BEGIN Setters
	public void addNode(T n) {this.vertices.add(n);}

	public void addEdge(Pair<T, T> edge) throws Exception {
		boolean isVertex0InGraph = this.vertices.contains(edge.getElement0());
		boolean isVertex1InGraph = this.vertices.contains(edge.getElement1());
		if (isVertex0InGraph && isVertex1InGraph) {
			this.edges.add(edge);
		} else {
			String notInGraph = "";
			if (!isVertex0InGraph) 
				notInGraph += edge.getElement0().toString() + " ";
			if (!isVertex1InGraph)
				notInGraph += edge.getElement1().toString();
			throw new Exception(String.format("The following nodes are not in the graph: %s", notInGraph));
		}
	}
	//END Setters

	//BEGIN Getters
	public boolean isConnected() {return numConnComponents() == 1;}

	public int numConnComponents() {
		int componentsConnected = 1;
//		HashMap<T, Boolean> spanningTree = breadthFirstSearch(false);
//		if (!this.connected.containsValue(spanningTree)) 
//			breadthFirstSearch(false);
		componentsConnected = this.connected.size();
		return componentsConnected;
	}
//	(iii) Graph[] connectedComponents() - returns a list of subgraphs of this graph, that are connected components.
	//TODO
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Graph[] connectedComponents() {
		int size = this.connected.size();
		Iterator<Entry<Integer, HashMap<T, Boolean>>> itr = this.connected.entrySet().iterator();
		Graph[] graph = new Graph[size];
		for (int i = 0; i < size; i++) {
			if (itr.hasNext()) {
				HashMap<T, Boolean> spanningTree = itr.next().getValue();
				Set<T> newVertices = spanningTree.keySet();
				Set<Pair<T,T>> newEdges = new HashSet<Pair<T,T>>();
				for (T element : newVertices) {
					newEdges.addAll(getIncidentEdges(element));
				}
				graph[i] = new Graph(newVertices, newEdges);
			}
		}
		return graph;
	}
	//END Getters
	
	//BEGIN Printers
	public void printBFS() {
		if (!this.connected.isEmpty())
			resetVisited();
		breadthFirstSearch(true);
	}
	
	public void printDFS() {
		if (!this.connected.isEmpty())
			resetVisited();
		depthFirstSearch(true, this.vertices);
	}

	public String toString() {
		String result = "";
		String vertices = "Nodes: ";
		String edges = "Edges: ";
		
		for (T v : this.vertices) {
			vertices += String.format("%s, ", v.toString());
		}
		for (Pair<T,T> e : this.edges) {
			edges += String.format("%s, ", e.toString());
		}
		result += vertices.substring(0, vertices.length()-2)
				+ "\n" + edges.substring(0, edges.length()-2);
		return result;
	}
	//END Printers
	
	//BEGIN HELPER FUNCTIONS
	protected HashMap<T, Boolean> depthFirstSearch(Set<T> node) {
		return depthFirstSearch(false, node);
	}
	protected HashMap<T, Boolean> depthFirstSearch(boolean print, Set<T> vertices) {
		Stack<T> s = new Stack<T>();
		HashMap<T, Boolean> visited = new HashMap<T, Boolean>();
		boolean done = false;
		for (T o : vertices)
		     visited.put(o, false);
		
		Iterator<Entry<T, Boolean>> itr = visited.entrySet().iterator();
		if (print)
			System.out.print("Depth First Search:   \n");
		while (!done) {
			done = allVisited(visited);
		     if (itr.hasNext()) {
		          Map.Entry<T, Boolean> element = (Map.Entry<T, Boolean>)itr.next();
		          if ((Boolean)element.getValue() == false) {
		        	  element.setValue(true);
		               T u = (T)element.getKey();
		               s.push(u);
		               if (print)
		            	   System.out.print(String.format("%s ", s.peek().toString()));
		          }
		          while (!s.isEmpty()) {
		        	  T current = s.peek();
		        	  Set<Pair<T,T>> currentEdges = getIncidentEdges(current);
		        	  boolean added = false;
		               for (Pair<T, T> p : currentEdges) {
		            	   T a = p.getElement0();
		            	   T b = p.getElement1();
		            	   if (current == a && !s.contains(b) && !visited.get(b)) {
		            		   s.push(b);
		            		   added = true;
		            	   } else if (current == b && !s.contains(a) && !visited.get(a)) {
		            		   s.push(a);
		            		   added = true;
		            	   }
		            	   if (added && !visited.get(s.peek())) {
		            		   visited.put(s.peek(), true);
		            		   if (print)
		            			   System.out.print(String.format("%s ", s.peek().toString()));
		            	   }
		               }
		               if (!added)
		            	   s.pop();
		          }
		          HashMap<T, Boolean> component = new HashMap<T, Boolean>();
		          for (Map.Entry<T, Boolean> obj : visited.entrySet())
		        	  if (obj.getValue()) {
		        		  component.put(obj.getKey(), obj.getValue());
//		        		  visited.remove(obj.getKey());
		        	  }
		          if (print)
		        	  System.out.println();
		          addConnected(component);
		     }
		}
		if (print)
			System.out.println();
//		addConnected(visited);
		return visited;
	}
	
	protected HashMap<T, Boolean> breadthFirstSearch(boolean print) {
		HashMap<T, Boolean> visited = new HashMap<T, Boolean>();
		Queue<T> q = new LinkedList<T>();
		boolean done = false;
		Iterator<Entry<Integer, HashMap<T, Boolean>>> cItr = this.connected.entrySet().iterator();
		if (cItr.hasNext()) {
			Map.Entry<Integer, HashMap<T, Boolean>> connectedComponentElement = 
					(Map.Entry<Integer, HashMap<T, Boolean>>) cItr.next();
			HashMap<T, Boolean> componentValue = connectedComponentElement.getValue();
			visited.putAll(componentValue);
		}
		for (T o : this.vertices) {
			if (!visited.containsKey(o)) {
				visited.put(o, false);
			} else {
				visited.remove(o, true);
			}
		}
		Iterator<Entry<T, Boolean>> itr = visited.entrySet().iterator();
		if (print)
			System.out.print("Breadth First Search: ");
		while (!done) {
			done = allVisited(visited);
			if (itr.hasNext()) {
				Map.Entry<T, Boolean> element = (Map.Entry<T, Boolean>)itr.next();
				if ((Boolean)element.getValue() == false) {
					element.setValue(true);
					T u = (T)element.getKey();
					q.offer(u);
				}
				while (!q.isEmpty()) {
					T current = q.poll();
					if (print)
						System.out.print(String.format("%s ", current.toString()));
					for (Pair<T, T> p : this.edges) {
						T a = p.getElement0();
						T b = p.getElement1();
						if (current == a || current == b) {
							if (!visited.get(a)) {
								visited.put(a, true);
								q.offer(a);
							}
							if (!visited.get(b)) {
								visited.put(b, true);
								q.offer(b);
							}
						}
					}
				}
			}
		}
		if (print)
			System.out.println();
		addConnected(visited);
		return visited;
	}
	
	protected void addConnected( HashMap<T, Boolean> visited) {
//		if (!this.connected.isEmpty())
//			resetVisited(visited);
		if (!visited.isEmpty() && !this.connected.containsValue(visited)) {
//			HashMap<T, Boolean> component = new HashMap<T, Boolean>();
//			for (Entry<T, Boolean> theThing : visited.entrySet())
//				if (theThing.getValue() && !this.connected.containsKey(theThing.getKey()))
//					component.put(theThing.getKey(), theThing.getValue());
			this.connected.put(this.connected.size() + 1, visited);
		}
	}
	
	private boolean allVisited(HashMap<T, Boolean> hm) {
		for (Boolean b : hm.values())
			if (b == false)
				return false;
		return true;
	}
	
	private void resetVisited() {
		for (Map.Entry<Integer, HashMap<T, Boolean>> hm : this.connected.entrySet()) {
			HashMap<T, Boolean> component = hm.getValue();
			for (Map.Entry<T, Boolean> element : component.entrySet())
				element.setValue(false);
		}
	}
	
	private void resetVisited(HashMap<T, Boolean> visited) {
		for (Map.Entry<T, Boolean> hm : visited.entrySet())
			hm.setValue(false);
	}
	
	protected Set<Pair<T,T>> getIncidentEdges(T node) {
		Set<Pair<T,T>> result = new HashSet<Pair<T,T>>();
		for (Pair<T,T> p : this.edges) {
			if (p.getElement0() == node || p.getElement1() == node)
				result.add(p);
		}
		return result;
	}
	protected Set<T> getVertices() {return this.vertices;};
	protected Set<Pair<T, T>> getEdges() {return this.edges;};
	protected Map<Integer, HashMap<T, Boolean>> getConnected() {return this.connected;};
	//END HELPER FUNCTIONS
}
