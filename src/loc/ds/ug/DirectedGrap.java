package loc.ds.ug;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.Map.Entry;

public class DirectedGrap<T> extends Grap<T> {
	private Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> stronglyConnected;
	public DirectedGrap() {
		super();
		this.stronglyConnected = new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
	}
	public DirectedGrap(Set<T> vertices, Set<Pair<T,T>> edges) {
		super(vertices, edges);
		this.stronglyConnected = new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
	}
	public boolean isConnected() {
		return super.isConnected();
	}
	
	public boolean isStronglyConnected() {
//		if (methodTwo())
		this.stronglyConnected.putAll(depthFirstSearch(this));
		return this.stronglyConnected.size() == 1;
	}
	
	public int numConnComponents() {
		return this.stronglyConnected.size();
	}
	
	public void printBFS() {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components = breadthFirstSearch(this);
		for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : components.entrySet()) 
			for (Set<T> nodes : component.getValue().keySet())
				System.out.printf("Tree: %s\n", nodes.toString());
	}
	public void printDFS() {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components = depthFirstSearch(this);
		for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : components.entrySet()) 
			for (Set<T> nodes : component.getValue().keySet())
				System.out.printf("Tree: %s\n", nodes.toString());
	}
	
	public DirectedGrap<T>[] connectedComponents() {
		if (!this.isStronglyConnected()) {
			DirectedGrap<T>[] grappies = new DirectedGrap[numConnComponents()];
			for (Map.Entry<Integer, Map<Set<T>, Set<Pair<T,T>>>> component : this.stronglyConnected.entrySet()) {
				for (Map.Entry<Set<T>, Set<Pair<T,T>>> g : component.getValue().entrySet()) {
					grappies[component.getKey()-1] = new DirectedGrap(g.getKey(), g.getValue());
				}
			}
			return grappies;
		} else
			return null;
	}
	private Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> breadthFirstSearch(DirectedGrap<T> g) {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components =
				new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();

		Queue<T> queue = new LinkedList<T>();
		Map<T, Boolean> visited = new HashMap<T, Boolean>();
		
		g.getVertices().forEach((T) -> {
			visited.put(T, false);
		});
		
		Iterator<Entry<T, Boolean>> vItr = visited.entrySet().iterator();
		while (vItr.hasNext()) {
			Entry<T, Boolean> u = vItr.next();
			T node = u.getKey();
			Boolean isVisited = u.getValue();
			Set<T> newVertices = new HashSet<T>();
			Set<Pair<T,T>> newEdges = new HashSet<Pair<T,T>>();
			Map<Set<T>, Set<Pair<T,T>>> spanningTree = new HashMap<Set<T>, Set<Pair<T,T>>>();
			if (!isVisited) {
				isVisited = true;
				visited.put(node, isVisited);
				queue.offer(node);
				newVertices.add(node);
				
				while (!queue.isEmpty()) {
					node = queue.poll();
					for (Pair<T,T> p : g.getEdges()) {
						T v0 = p.getElement0();
						T v1 = p.getElement1();
						if (v0 == node) {
							if (!visited.get(v1)) {
								visited.put(v1, true);
								queue.offer(v1);
								newVertices.add(v1);
								newEdges.add(p);
							} else if (!newEdges.contains(p))
								newEdges.add(p);
						}
//						if (v1 == node) {
//							if (!visited.get(v0)) {
//								visited.put(v0, true);
//								queue.offer(v0);
//								newVertices.add(v0);
//								newEdges.add(p);
//							} else if (!newEdges.contains(p))
//								newEdges.add(p);
//						}
					}
				}
				spanningTree.put(newVertices, newEdges);
				if (!components.containsValue(spanningTree)) {
					components.put(components.size()+1, spanningTree);
				}
			}
		}
		return components;
	}
	private Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> depthFirstSearch(DirectedGrap<T> g) {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> components =
				new HashMap<Integer, Map<Set<T>, Set<Pair<T,T>>>>();
		
		Stack<T> stack = new Stack<T>();
		Map<T, Boolean> visited = new HashMap<T, Boolean>();
		
		g.getVertices().forEach((T) -> {
			visited.put(T, false);
		});
		
		Iterator<Entry<T, Boolean>> vItr = visited.entrySet().iterator();
		while (vItr.hasNext()) {
			Entry<T, Boolean> u = vItr.next();
			T node = u.getKey();
			Boolean isVisited = u.getValue();
			Set<T> newVertices = new HashSet<T>();
			Set<Pair<T,T>> newEdges = new HashSet<Pair<T,T>>();
			Map<Set<T>, Set<Pair<T,T>>> spanningTree = new HashMap<Set<T>, Set<Pair<T,T>>>();
			
			if (!isVisited) {
				isVisited = true;
				visited.put(node, isVisited);
				stack.push(node);
				newVertices.add(node);
				
				while (!stack.isEmpty()) {
					node = stack.peek();
					boolean added = false;
					for (Pair<T,T> p : g.getEdges()) {
						T w = p.getElement0();
						T v = p.getElement1();
						if (w == node) {
							if (!visited.get(v)) {
								visited.put(v, true);
								stack.push(v);
								added = true;
								newVertices.add(v);
								newEdges.add(p);
							} 
							else if (!newEdges.contains(p))
								newEdges.add(p);
						} 
//						else if (v == node) {
//							if (!visited.get(w)) {
//								visited.put(w, true);
//								stack.push(w);
//								added = true;
//								newVertices.add(w);
//								newEdges.add(p);
//							} else if (!newEdges.contains(p))
//								newEdges.add(p);
//						}
					}
					if (!added)
						stack.pop();
				}
				spanningTree.put(newVertices, newEdges);
				if (!components.containsValue(spanningTree)) {
					components.put(components.size()+1, spanningTree);
				}
			}
		}
		return components;
	}
	private boolean methodTwo() {
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> properOrder =
				depthFirstSearch(this);
//		super.depthFirstSearch(super.getVertices());
		Set<Pair<T,T>> reversedEdges = reverseEdges(super.getEdges());
		DirectedGrap dReverse = new DirectedGrap(super.getVertices(), reversedEdges);
		Map<Integer, Map<Set<T>, Set<Pair<T,T>>>> reverseOrder =
				depthFirstSearch(dReverse);
		return properOrder.size() == reverseOrder.size();
	}
	private Set<Pair<T,T>> reverseEdges(Set<Pair<T,T>> edges) {
		Set<Pair<T,T>> reversed = new HashSet<Pair<T,T>>();
		
		for (Pair<T,T> p : edges) {
			T element0 = p.getElement1();
			T element1 = p.getElement0();
			Pair<T,T> edge = new Pair<T,T>(element0, element1);
			reversed.add(edge);
		}
		return reversed;
	}
}
